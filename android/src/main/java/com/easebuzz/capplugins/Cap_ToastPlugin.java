package com.easebuzz.capplugins;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.easebuzz.payment.kit.PWECouponsActivity;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.PluginResult;
import com.getcapacitor.annotation.CapacitorPlugin;

import static datamodels.PWEStaticDataModel.PWE_REQUEST_CODE;

@NativePlugin(
        requestCodes = {PWE_REQUEST_CODE}
)
@CapacitorPlugin(name = "Toast_cap")
public class Cap_ToastPlugin extends Plugin {


   /* @PluginMethod
    public void showToast(PluginCall call) {
        try {
            JSObject jsObject = call.getData();

            String value = call.getString("filter");
            Toast.makeText(getActivity(), "value", Toast.LENGTH_SHORT).show();
            Log.d("value :", value);
            call.success();
        } catch (Exception e) {
            Log.d("Error", e.getLocalizedMessage());
        }
    }*/

     @PluginMethod
     public void showToast(PluginCall call) {

         String accesskey = call.getString("accesskey");
         String paymode = call.getString("paymode");
//         if (text == null) {
//             call.reject("Must provide text");
//             return;
//         }

//         String durationType = call.getString("duration", "short");

//         int duration = android.widget.Toast.LENGTH_SHORT;
//         if ("long".equals(durationType)) {
//             duration = android.widget.Toast.LENGTH_LONG;
//         }
         Toast_cap.showt(getActivity(), "toast value : "+accesskey+"  :  "+paymode);

         Intent intent = new Intent(getActivity(), PWECouponsActivity.class);
         intent.putExtra("access_key", accesskey);
         intent.putExtra("pay_mode", paymode);
         startActivityForResult(call,intent, PWE_REQUEST_CODE);

         JSObject ret = new JSObject();
         ret.put("added", "sillyData!");
         call.resolve(ret);
         saveCall(call);
     }

    @Override
    protected void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        super.handleOnActivityResult(requestCode, resultCode, data);
        PluginCall lastSavedCall = getSavedCall();

        if (lastSavedCall == null) {
            Log.e("Test", "No stored plugin call for permissions request result");
//            return;
        }
        String recievedMessage = data.getStringExtra("Data");
        Log.e("data", recievedMessage+"");






        if (requestCode == PWE_REQUEST_CODE) {
            // We got the permission!

            PluginResult result = new PluginResult();
            result.put("data", recievedMessage);
            lastSavedCall.successCallback(result);
//            loadRes(savedCall);
        }
    }

//    void loadRes(PluginCall call) {
//
//    }

   /* @Override
    protected void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        super.handleOnActivityResult(requestCode, resultCode, data);
        final PluginCall lastSavedCall = getSavedCall();
        Toast_cap.showt(getActivity(), data.getData().toString());
                try {
                    JSObject jsObject = new JSObject();
                    try {
                        jsObject.put("response", data.getData().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("RESULT",jsObject.toString());


                    if (lastSavedCall == null){
                        Log.e("ERROR","no call saved");
                        return;
                    }
                    lastSavedCall.success(jsObject);
                }catch (Exception e){
                    e.printStackTrace();
                }

    }*/
}
