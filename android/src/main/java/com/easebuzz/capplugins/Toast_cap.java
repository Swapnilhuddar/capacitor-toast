package com.easebuzz.capplugins;

import android.content.Context;
import android.os.Looper;
import android.view.Gravity;

import java.util.logging.Handler;

public class Toast_cap {

    private static final int GRAVITY_TOP = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
    private static final int GRAVITY_CENTER = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;

    public static void showt(Context c, String text) {
        showt(c, text, android.widget.Toast.LENGTH_LONG);
    }

/*    public static void show(final Context c, final String text, final int duration) {
        show(c, text, duration, "bottom");
    }*/

    public static void showt(final Context c, final String text, final int duration) {
        android.widget.Toast toast = android.widget.Toast.makeText(c, text, duration);

        toast.show();
    }


}
