import { registerPlugin } from '@capacitor/core';

import type { Cap_ToastPlugin } from './definitions';

const Cap_Toast = registerPlugin<Cap_ToastPlugin>('Cap_Toast', {
  web: () => import('./web').then(m => new m.Cap_ToastWeb()),
});

export * from './definitions';
export { Cap_Toast };
