export interface Cap_ToastPlugin {
  // echo(options: { value: string }): Promise<{ value: string }>;
  showToast(filter: string): Promise<{ results: any[] }>;
}
